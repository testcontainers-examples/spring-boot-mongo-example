# example with gitlab ci runner

This application is very simple; it contains only a connection to a database.

Please find the following in the test related resources:

| class name                            | description                     |
|---------------------------------------|---------------------------------|
| [AppTests][main-test]                 | Actual class with all tests     |
| [MongoTestConfiguration][config-test] | Configuration to wire the tests |

[main-test]: src/test/java/org/testcontainers/gitlabci/AppTests.java
[config-test]: src/test/java/org/testcontainers/gitlabci/MongoTestConfiguration.java
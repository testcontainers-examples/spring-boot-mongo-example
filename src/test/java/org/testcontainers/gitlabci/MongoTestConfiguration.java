package org.testcontainers.gitlabci;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.testcontainers.containers.MongoDBContainer;

@Slf4j
@TestConfiguration
public class MongoTestConfiguration {

    private static final MongoDBContainer MONGO_DB_CONTAINER;
    private static final String MONGO_VERSION = "mongo:latest";

    static {
        MONGO_DB_CONTAINER = new MongoDBContainer(MONGO_VERSION);
        MONGO_DB_CONTAINER.start();
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        String mongoDatabaseName = "testcontainers-test";

        ConnectionString connectionString = new ConnectionString(MONGO_DB_CONTAINER.getReplicaSetUrl(mongoDatabaseName));

        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();

        MongoClient mongoClient = MongoClients.create(mongoClientSettings);

        return new MongoTemplate(mongoClient, mongoDatabaseName);
    }
}

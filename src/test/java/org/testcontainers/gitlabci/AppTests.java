package org.testcontainers.gitlabci;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.testcontainers.gitlabci.database.ContainerRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testcontainers.gitlabci.database.ContainerService.AWESOME_WEBSERVER;

@SpringBootTest
@Import(MongoTestConfiguration.class)
class AppTests {

    @Autowired
    private ContainerRepository repository;

    @Test
    @DisplayName("After startup the database should hold 2 documents")
    void test() {
        assertThat(repository.count())
                .isEqualTo(2);


    }

    @Test
    @DisplayName("Find one container")
    void testFindAwesomeWebserver() {
        var container = repository.findByName(AWESOME_WEBSERVER);
        assertThat(container.getName()).isEqualTo(AWESOME_WEBSERVER);
        assertThat(container.getVersion()).isEqualTo("1.0.0");
        assertThat(container.getDescription()).isEqualTo("an awesome webserver with everything that you need");
    }

}

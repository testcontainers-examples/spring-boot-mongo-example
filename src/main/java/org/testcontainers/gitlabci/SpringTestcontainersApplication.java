package org.testcontainers.gitlabci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class SpringTestcontainersApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringTestcontainersApplication.class, args);
    }

}

package org.testcontainers.gitlabci.database;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document(collection = "container")
public class ContainerDoc {
    private String name;
    private String version;
    private String description;

    @Builder
    public static ContainerDoc of(String name, String version, String description) {
        var containerDoc = new ContainerDoc();
        containerDoc.setName(name);
        containerDoc.setVersion(version);
        containerDoc.setDescription(description);
        return containerDoc;
    }
}

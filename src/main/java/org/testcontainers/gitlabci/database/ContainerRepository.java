package org.testcontainers.gitlabci.database;

import org.springframework.data.repository.PagingAndSortingRepository;


public interface ContainerRepository extends PagingAndSortingRepository<ContainerDoc, String> {
    ContainerDoc findByName(String name);
}

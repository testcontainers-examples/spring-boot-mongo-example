package org.testcontainers.gitlabci.database;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContainerService {
    public static final String AWESOME_WEBSERVER = "awesome-webserver";
    private final ContainerRepository repository;

    @EventListener(ApplicationReadyEvent.class)
    public void fillDatabaseOnStart() {
        var webContainer = ContainerDoc.builder().name(AWESOME_WEBSERVER)
                .version("1.0.0")
                .description("an awesome webserver with everything that you need")
                .build();

        var langContainer = ContainerDoc.builder().name("awesome-programming-language")
                .version("2.0.0")
                .description("best programming language ever")
                .build();

        Stream.of(webContainer, langContainer).forEach(repository::save);

        log.info("Containers have been stored");
    }

}
